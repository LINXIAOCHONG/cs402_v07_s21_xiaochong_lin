package sp;

enum SeparationLineDirection {
	vertical_X,
	horizontal_Y;			
}

public class SeparationLineNode {
	private SeparationLineDirection m_sld;
	private float m_separationLineValue=0;
	private int m_separationLinePosition=0;
	
	public SeparationLineDirection getSeparationLineDirection() {
		return m_sld;
	}
	public void setSeparationLineDirection(SeparationLineDirection sld) {
		m_sld = sld;
	}
	public float getSeparationLineValue() {
		return m_separationLineValue;
	}
	public void setSeparationLineValue(float separationLineValue) {
		m_separationLineValue = separationLineValue;
	}
	public int getSeparationLinePosition() {
		return m_separationLinePosition;
	}
	public void setSeparationLinePosition(int position) {
		m_separationLinePosition = position;
	}
	//constructor
	public SeparationLineNode(float slv, SeparationLineDirection sld, int position) {
		m_sld = sld;
		m_separationLineValue = slv;
		m_separationLinePosition = position;
	}
}