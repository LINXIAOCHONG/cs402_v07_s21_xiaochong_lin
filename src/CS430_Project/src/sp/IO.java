package sp;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.LinkedList;

public class IO {
	static String fileNum;
	
	public static LinkedList<String> LoadFile() throws IOException {
		LinkedList<String> linkedListData = new LinkedList<String>();
		try {
			FileInputStream inputStream = new FileInputStream("src\\sp\\instance" + fileNum + ".txt");
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			
			String str = null;
			while((str = bufferedReader.readLine()) != null)
			{
				linkedListData.add(str);
			}
				
			inputStream.close();
			bufferedReader.close();
			

		} catch (IOException e) {
			System.out.println("file instance" + fileNum + " not found!");
		}
		return linkedListData;
	}
    public static void WriteTxt(ArrayList<SeparationLineNode> S) throws FileNotFoundException {
        PrintStream printStream = new PrintStream(new FileOutputStream("src\\sp\\greedy_solution" + fileNum + ".txt"));
        printStream.println(S.size());
		for (SeparationLineNode separationLineNode : S) {
			switch (separationLineNode.getSeparationLineDirection()) {
			case horizontal_Y:
				printStream.print("h ");
				break;
			case vertical_X:
				printStream.print("v ");
				break;
			}
			printStream.println(separationLineNode.getSeparationLineValue());
		}
        printStream.close();
    }
}