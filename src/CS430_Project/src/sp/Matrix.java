package sp;

public class Matrix {
	private Pixel[][] pixelArray;
	private Pixel[][] lineArray;
	private MatrixNode[][] matrixNodeArray;
	private MatrixNode biggestMatrixNode;
	private int m_size;
	private boolean m_done;
	
	//getter
	public Pixel[][] getPixelArray(){
		return pixelArray;
	}
	public Pixel[][] getLineArray(){
		return lineArray;
	}
	public int getSize() {
		return m_size;
	}
	public MatrixNode[][] getMatrixNodeArray(){
		return matrixNodeArray;
	}
	public MatrixNode getBiggestMatrixNode() {
		return biggestMatrixNode;
	}
	public boolean isDone() {
		return m_done;
	}
	//setter
	public void setPixelArray(Pixel p) {
		pixelArray[p.getX()-1][p.getY()-1] = p;
	}
	public void setLineArray(Pixel p, int x, int y) {
		lineArray[x][y] = p;
	}
	
	//constructor:
	public Matrix(int size) {
		m_size = size;
		pixelArray = new Pixel[size][size];
		lineArray = new Pixel[size][size];
		matrixNodeArray = new MatrixNode[1][1];
	}
	
	public void RebuildMatrixNodeArray(SeparationLineSet separationLineSet) {
		int v = separationLineSet.getVerticalArray().size();
		int h = separationLineSet.getHorizontalArray().size();
		matrixNodeArray = new MatrixNode[h][v];
		int prevX=0, prevY=0;
		for (int i=0;i<h;i++) {
			for (int j=0;j<v;j++) {
				matrixNodeArray[i][j] = new MatrixNode(prevX, separationLineSet.getVerticalArray().get(j).getSeparationLinePosition(), 
						prevY, separationLineSet.getHorizontalArray().get(i).getSeparationLinePosition(), j, i, this);
				prevX = separationLineSet.getVerticalArray().get(j).getSeparationLinePosition()+1;
			}
			prevX = 0;
			prevY = separationLineSet.getHorizontalArray().get(i).getSeparationLinePosition()+1;
		}
		if (!CheckPendingMatrixNodeDone()) {
			GetMatrixNodeDirection(v, h);			
		}
	}
	
	protected boolean CheckPendingMatrixNodeDone() {
		m_done = true;
		for (MatrixNode[] mn: matrixNodeArray) {
			for (MatrixNode mn1: mn) {
				if (mn1.getPixelQuantity()>1) {
					m_done = false;
					break;
				}
			}
		}
		return m_done;
	}
	
	protected void GetMatrixNodeDirection(int vLineSize, int hLineSize) {
		int countX=0, countY=0;
		int maxCountX=0, maxCountY=0;
		MatrixNode nodeX=null, nodeY=null;
		//check x
		for (int i=0;i<hLineSize;i++) {
			for (int j=0; j<vLineSize; j++) {
				if (matrixNodeArray[i][j].getPixelQuantity()>1) {
					countX += matrixNodeArray[i][j].getPixelQuantity();
				}				
			}
			if (countX > maxCountX) {
				maxCountX = countX;
				nodeX = matrixNodeArray[i][0];
			}
			countX = 0;
		}
		//check y
		for (int j=0;j<vLineSize;j++) {
			for (int i=0;i<hLineSize;i++) {
				if (matrixNodeArray[i][j].getPixelQuantity()>1) {
					countY += matrixNodeArray[i][j].getPixelQuantity();
				}				
			}
			if (countY > maxCountY) {
				maxCountY = countY;
				nodeY = matrixNodeArray[0][j];
			}
			countY = 0;
		}
		
		if (maxCountX == maxCountY) {
			if (vLineSize > hLineSize) {
				nodeX.setSeparationLineDirection(SeparationLineDirection.horizontal_Y);
				biggestMatrixNode = nodeX;
			}
			else {
				nodeY.setSeparationLineDirection(SeparationLineDirection.vertical_X);
				biggestMatrixNode = nodeY;
			}				
		}
		else {
			if (maxCountX > maxCountY) {
				nodeX.setSeparationLineDirection(SeparationLineDirection.horizontal_Y);
				biggestMatrixNode = nodeX;
			}
			else {
				nodeY.setSeparationLineDirection(SeparationLineDirection.vertical_X);
				biggestMatrixNode = nodeY;
			}			
		}
	}
	
	public void PrintMatrix() {
		System.out.println("--------------------------------------------------");
		for (int y = 0; y<m_size;y++) {
			for (int x=0; x<m_size;x++) {
				if (pixelArray[x][y] != null){
					System.out.print(String.format("%-5s", pixelArray[x][y].getX() + ":" + pixelArray[x][y].getY()));
				}
				else {
					if (lineArray[x][y] != null){
						if (lineArray[x][y].getX()==0) {
							System.out.print(String.format("%-5s", "    " + "\u007C"));						
						}
						else {
							System.out.print(String.format("%-5s", "    " + "/"));						
						}
					}
					else {
						System.out.print(String.format("%-5s", "    " + "\u007C"));
					}						
				}
			}
			System.out.println();
			for (int x=0; x<m_size;x++) {
				if (lineArray[x][y] != null){
					if (lineArray[x][y].getY()==0) {
						System.out.print(String.format("%-1s", "-----"));						
					}
					else {
						System.out.print(String.format("%-1s", "====="));						
					}
				}
				else {
					System.out.print(String.format("%-1s", "-----"));						
				}
			}
			System.out.println();			
		}		
	}
}
