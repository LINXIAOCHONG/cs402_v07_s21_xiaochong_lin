package sp;

import java.util.ArrayList;

public class MatrixNode {
	private int m_startX;
	private int m_startY;
	private int m_endX;
	private int m_endY;
	private Matrix m_matrix;
	private int m_pq;
	private SeparationLineDirection m_d;
	private int m_subMatrixX;
	private int m_subMatrixY;
	private ArrayList<Pixel> m_pixelArray;
	
	//getter
	public int getStartX() {
		return m_startX;
	}
	public int getStartY() {
		return m_startY;
	}
	public int getEndX() {
		return m_endX;
	}
	public int getEndY() {
		return m_endY;
	}
	public SeparationLineDirection getSeparationLineDirection() {
		return m_d;
	}
	public Matrix getMatrix() {
		return m_matrix;
	}
	public int getPixelQuantity() {
		return m_pq;
	}
	public int getSubMatrixX() {
		return m_subMatrixX;
	}
	public int getSubMatrixY() {
		return m_subMatrixY;
	}
	public ArrayList<Pixel> getSubPixelArray(){
		return m_pixelArray;
	}
	//setter
	public void setSeparationLineDirection(SeparationLineDirection separationLineDirection) {
		m_d = separationLineDirection;
	}

	public MatrixNode(int sx, int ex, int sy, int ey, int subMatrixX, int subMatrixY, Matrix M) {
		super();
		this.m_startX = sx;
		this.m_endX = ex;
		this.m_startY = sy;
		this.m_endY = ey;
		m_subMatrixX = subMatrixX;
		m_subMatrixY = subMatrixY;
		m_matrix = M;
		CheckMatrixNode();
	}	
	protected void CheckMatrixNode() {
		m_pixelArray = new ArrayList<Pixel>();
		int count=0;
		//check pixel
		for (int i=m_startY; i<m_endY+1; i++) {
			for (int j=m_startX; j<m_endX+1; j++) {
				if (m_matrix.getPixelArray()[j][i]!=null) {
					m_pixelArray.add(m_matrix.getPixelArray()[j][i]);
					count++;
				}
			}
		}
		m_pq = count;
	}
}
