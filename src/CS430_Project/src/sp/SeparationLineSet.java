package sp;

import java.util.ArrayList;
import java.util.Comparator;

public class SeparationLineSet {
	private ArrayList<SeparationLineNode> S = new ArrayList<SeparationLineNode>();
	private ArrayList<SeparationLineNode> horizontalArray = new ArrayList<SeparationLineNode>();
	private ArrayList<SeparationLineNode> verticalArray = new ArrayList<SeparationLineNode>();
	
	public ArrayList<SeparationLineNode> getHorizontalArray(){
		return horizontalArray;
	}
	public ArrayList<SeparationLineNode> getVerticalArray(){
		return verticalArray;
	}
	public ArrayList<SeparationLineNode> getSet(){
		return S;
	}
	
	public void GetSeparationLine(MatrixNode matrixNode) {
		SeparationLineNode separationLineNode = FindSeparationLine(matrixNode);
		if (separationLineNode.getSeparationLinePosition() >= 0) {
			S.add(separationLineNode);
			switch (matrixNode.getSeparationLineDirection()) {
			case horizontal_Y:
				horizontalArray.add(separationLineNode);
				horizontalArray.sort(cmpPositoin);
				break;
			case vertical_X:
				verticalArray.add(separationLineNode);
				verticalArray.sort(cmpPositoin);
				break;
			}
		}
	}
	
	public void PrintSeparationLineSet() {
		System.out.println(S.size());
		for (SeparationLineNode separationLineNode : S) {
			switch (separationLineNode.getSeparationLineDirection()) {
			case horizontal_Y:
				System.out.print("h ");
				break;
			case vertical_X:
				System.out.print("v ");
				break;
			}
			System.out.println(separationLineNode.getSeparationLineValue());
		}
	}
	
	static Comparator<Pixel> cmpPositoinY = new Comparator<Pixel>() {
		@Override
		public int compare(Pixel arg0, Pixel arg1) {
			return arg0.getY() - arg1.getY();
		}
	};
	
	static Comparator<Pixel> cmpPositoinX = new Comparator<Pixel>() {
		@Override
		public int compare(Pixel arg0, Pixel arg1) {
			return arg0.getX() - arg1.getX();
		}
	};
	
	static Comparator<SeparationLineNode> cmpPositoin = new Comparator<SeparationLineNode>() {
		@Override
		public int compare(SeparationLineNode arg0, SeparationLineNode arg1) {
			return arg0.getSeparationLinePosition() - arg1.getSeparationLinePosition();
		}
	};
	
	protected SeparationLineNode FindSeparationLine(MatrixNode matrixNode) {
		float middle = -1;
		MatrixNode[][] matrixNodeArray = matrixNode.getMatrix().getMatrixNodeArray();
		ArrayList<Pixel> combinedPixelArray = new ArrayList<Pixel>();
		int positionX = matrixNode.getSubMatrixX();
		int positionY = matrixNode.getSubMatrixY();
		int m1=-1, m2=-1, count=0, position=0;
		switch (matrixNode.getSeparationLineDirection()) {
		case horizontal_Y:
			for (int i=0;i<matrixNodeArray[0].length;i++) {
				if (matrixNodeArray[positionY][i].getPixelQuantity()>1) {
					combinedPixelArray.addAll(matrixNodeArray[positionY][i].getSubPixelArray());		
				}
			}
			combinedPixelArray.sort(cmpPositoinY);
			count = combinedPixelArray.size();
			if (count > 1) {
				if ((count&1)==1) {
					m1 = (int)Math.floor(count/2);
				}
				else {
					m1 = count/2;
				}
				m2 = m1+1;
				position = combinedPixelArray.get(m1-1).getY()-1;
				middle = (float)(combinedPixelArray.get(m1-1).getY()+combinedPixelArray.get(m2-1).getY())/2;
			}
			break;
		case vertical_X:
			for (int i=0;i<matrixNodeArray.length;i++) {
				if (matrixNodeArray[i][positionX].getPixelQuantity()>1) {
					combinedPixelArray.addAll(matrixNodeArray[i][positionX].getSubPixelArray());					
				}
			}
			combinedPixelArray.sort(cmpPositoinX);
			count = combinedPixelArray.size();
			if (count > 1) {
				if ((count&1)==1) {
					m1 = (int)Math.floor(count/2);
				}
				else {
					m1 = count/2;
				}
				m2 = m1+1;
				position = combinedPixelArray.get(m1-1).getX()-1;
				middle = (float)(combinedPixelArray.get(m1-1).getX()+combinedPixelArray.get(m2-1).getX())/2;
			}
			break;
		}
		PaintMatrixSeparationLine(matrixNode, position);
		return new SeparationLineNode(middle, matrixNode.getSeparationLineDirection(), position);
	}
	protected void PaintMatrixSeparationLine(MatrixNode matrixNode, int position) {
		Matrix M = matrixNode.getMatrix();
		switch (matrixNode.getSeparationLineDirection()) {
		case horizontal_Y:
			for (int i = 0; i<M.getSize(); i++) {
				M.setLineArray(new Pixel(0, 1), i, position);
			}
			break;
		case vertical_X:
			for (int i = 0; i<M.getSize(); i++) {
				M.setLineArray(new Pixel(1, 0), position, i);
			}
			break;
		}
	}
}