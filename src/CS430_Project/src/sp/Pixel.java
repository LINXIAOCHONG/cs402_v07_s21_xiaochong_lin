package sp;

public class Pixel {
	private int m_x;
	private int m_y;
	public int getX() {
		return m_x;
	}
	public int getY() {
		return m_y;
	}
	public void setX(int x) {
		m_x = x;
	}
	public void setY(int y) {
		m_y = y;
	}
	
	//constructor:
	public Pixel(int x, int y) {
		m_x = x;
		m_y = y;
	}
}