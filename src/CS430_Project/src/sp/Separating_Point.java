package sp;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;

public class Separating_Point {

	public static void main(String[] args) throws IOException {
		Scanner scan = new Scanner(System.in);
		int fileCount = 1;
        do {
            //System.out.println("Enter number of file(01..99):");
            //System.out.println("Enter 'x' to quit.");
            //IO.fileNum = scan.nextLine();
    		
    		LinkedList<String> linkedListData = new LinkedList<String>();
    		IO.fileNum = String.format("%02d", fileCount);
    		//if (IO.fileNum.compareToIgnoreCase("x") != 0) {
        	//if (IO.fileNum.compareToIgnoreCase("x") != 0) {
        	linkedListData = IO.LoadFile();    			
    		//}
    		
    		if (linkedListData.size()>0) {
        		int count = 0;
        		count = Integer.parseInt(linkedListData.removeFirst());
        		Matrix M = new Matrix(count);
        		for (String s : linkedListData){
        			String[] sa = s.split(" ");
        			int x = Integer.parseInt(sa[0]);
        			int y = Integer.parseInt(sa[1]);
        			M.setPixelArray(new Pixel(x, y));
        		}
        		
        		SeparationLineSet separationLineSet = new SeparationLineSet();
        		separationLineSet.getHorizontalArray().add(new SeparationLineNode(-1, SeparationLineDirection.horizontal_Y, M.getSize()-1));
        		separationLineSet.getVerticalArray().add(new SeparationLineNode(-1, SeparationLineDirection.vertical_X, M.getSize()-1));
        		
        		do {
        			M.RebuildMatrixNodeArray(separationLineSet);
        			if (!M.isDone()) {
        				separationLineSet.GetSeparationLine(M.getBiggestMatrixNode());
        			}
        		} while (!M.isDone());
        		//M.PrintMatrix();
        		IO.WriteTxt(separationLineSet.getSet());
        		separationLineSet.PrintSeparationLineSet();       			
    		}
    		fileCount++;
        } while (fileCount < 100);     	
        //} while (IO.fileNum.compareToIgnoreCase("x")!=0);
        scan.close();
	}
}