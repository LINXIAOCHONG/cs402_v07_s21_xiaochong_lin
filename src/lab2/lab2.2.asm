	.data 0x10010000
var1: 	.word 0x53 	# var1 is a word (32 bit) with the ..
			# initial value 83
var2: 	.word 0x68	# var2 is a word (32 bit) with the ..
			# initial value 104
var3:	.word 0x6f	# var3 is a word (32 bit) with the ..
			# initial value 111
var4:	.word 0x77	# var4 is a word (32 bit) with the ..
			# initial value 119
first:	.byte 'C'		# first is a byte(8 bit) with the initial value 'C' ASCII 67
last:	.byte 'L'		# last is a byte(8 bit) with the initial value 'L' ASCII 76
	.text
	.globl main
main: 	addu $s0, $ra, $0 	# save $31 in $16

#load the variables to the regs
	lw $t1, var1	#load var1 to $t1
	lw $t2, var2	#load var2 to $t2
	lw $t3, var3	#load var3 to $t3
	lw $t4, var4	#load var4 to $t4

	lbu $t5, first	#load first to $t5
	lbu $t6, last	#load last to $t6

#store the regs by swaps to the variables
	sw $t4, var1	#store $t4 to var1
	sw $t1, var4	#store $t1 to var4
	sw $t3, var2	#store $t3 to var2
	sw $t2, var3	#store $t2 to var3

	sb $t5, last	#store $t5 to last
	sb $t6, first	#store $t6 to first

# restore now the return address in $ra and return from main
	addu $ra, $0, $s0 	# return address back in $31
	jr $ra 		# return from main