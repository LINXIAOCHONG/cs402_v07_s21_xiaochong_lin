	.data 0x10010000
var1: 	.word 0x23 	# var1 is a word (32 bit) with the ..
			# initial value 35
var2: 	.word 0x16	# var2 is a word (32 bit) with the ..
			# initial value 22
	.extern ext1, 4	# ext1 is a global word(32 bit)			
	.extern ext2, 4	# ext2 is a global word (32 bit)
			
	.text
	.globl main
main: 	addu $s0, $ra, $0 	# save $31 in $16

#load the variables to the regs	
	lw $t1, var1	#load var1 to $t1
	sw $t1, 4($gp)	#store $t1 to ext2
	lw $t2, var2	#load var2 to $t2
	sw $t2, 0($gp)	#store $t2 to ext1

# restore now the return address in $ra and return from main
	addu $ra, $0, $s0 	# return address back in $31
	jr $ra 		# return from main