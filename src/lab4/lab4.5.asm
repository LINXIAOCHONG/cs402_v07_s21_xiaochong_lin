	.data
msg:	.asciiz "the value must be positive!\n"	# initial a warning string
msgrst:	.asciiz "the factorial result is:\n"		# initial a result string
	.text
	.globl main
main: 	move $s0, $ra	# $s0 <- $ra, save the return address

read:	li $v0, 5		# $v0 <- 5, load immediate value 5 for read int
	syscall
	move $t0, $v0	# $t0 <- $v0, copy the value from $v0 to $t0

	blt $t0, $0, warning	# if ($t0 < $0) jump to label warning	
	jal Factorial
	
	li $v0, 4		# $v0 <- 4, load immediate value 4 for print a string
	la $a0, msgrst	# $a0 <- address of the result string
	syscall
	li $v0, 1		# $v0 <- 4, load immediate value 4 for print a integer
	move $a0, $t1	# $a0 <- copy $v0 to $a0
	syscall
	
	move $ra, $s0	# $ra <- $s0, restore the return address after the procedure
	jr $ra		# jump to exit
warning:	
	li $v0, 4		# $v0 <- 4, load immediate value 4 for print a string
	la $a0, msg	# $a0 <- address of the warning string
	syscall
	j read		# jump to label read
Factorial:
	subu $sp, $sp, 4
	sw $ra, 4($sp) 	# save the return address on stack
	beqz $t0, terminate 	# test for termination
	subu $sp, $sp, 4 	# do not terminate yet
	sw $t0, 4($sp) 	# save the parameter
	sub $t0, $t0, 1 	# will call with a smaller argument
	jal Factorial
# after the termination condition is reached these lines
# will be executed
	lw $t0, 4($sp) 	# the argument I have saved on stack
	mul $t1, $t1, $t0 	# do the multiplication
	lw $ra, 8($sp) 	# prepare to return
	addu $sp, $sp, 8 	# I’ve popped 2 words (an address and
	jr $ra 		# .. an argument)
terminate:
	li $t1, 1 		# 0! = 1 is the return value
	lw $ra, 4($sp) 	# get the return address
	addu $sp, $sp, 4 	# adjust the stack pointer
	jr $ra 		# return