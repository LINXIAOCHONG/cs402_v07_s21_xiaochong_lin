	.text
	.globl main
main: 	add $sp, $sp, -4	# $sp <- $sp -4, $sp move one word lower
	sw $ra, 4($sp) 	# $sp + 4 <- $ra, store the $ra into stack
	jal test 		# call ‘test’ with no parameters
	nop 		# execute this after ‘test’ returns
	lw $ra, 4($sp) 	# $ra <- $sp + 4, restore the return address from $sp + 0 to $ra
	addu $sp, $sp, 4	# $sp <- $sp + 4, stack pop, move one word higher
	jr $ra 		# return from main
# The procedure ‘test’ does not call any other procedure. Therefore $ra
# does not need to be saved. Since ‘test’ uses no registers there is
# no need to save any registers.
test: 	nop 		# this is the procedure named ‘test’
	jr $ra 		# return from this procedure