		.data
msgX:		.asciiz "Please enter a non-negative integer for x\n"	# initial a prompt string
msgY:		.asciiz "Please enter a non-negative integer for y\n"	# initial a prompt string
msgResult:	.asciiz "The resut of A(x, y) is:\n"		# initial a result string
msgRec:		.asciiz "The recusive call number is:\n"		# initial a recursion call string
warning:		.asciiz "You should enter a non-negative integer\n"	# initial a warning string
		.text
		.globl main
main: 		move $s0, $ra		# $s0 <- $ra, save the return address

read:		li $v0, 4			# $v0 <- 4, load immediate value 4 for print a string
		la $a0, msgX		# $a0 <- address of the result string
		syscall
		li $v0, 5			# $v0 <- 5, load immediate value 5 for read int
		syscall
		move $t0, $v0		# $t0 <- $v0, copy the value from $v0 to $t0
		li $v0, 4			# $v0 <- 4, load immediate value 4 for print a string
		la $a0, msgY		# $a0 <- address of the result string
		syscall
		li $v0, 5			# $v0 <- 5, load immediate value 5 for read int
		syscall
		move $t1, $v0		# $t0 <- $v0, copy the value from $v0 to $t1
#in ‘main’ prompts the user to enter two non-negative integers; store them in $t0 and $t1

		blt $t0, $0, warn
		blt $t1, $0, warn
#check if any of the numbers entered is negative: if it is negative, then print a message saying so and prompt the user again for numbers
		
		jal Ackermann
# call the procedure named ‘Ackermann’ whose parameters will be the integers read from the user, and which returns the value of the Ackermann’s function for those two integers

		li $v0, 4			
		la $a0, msgResult		
		syscall
		li $v0, 1			
		move $a0, $t1		
		syscall
#prints a message and the value returned by ‘Ackermann’

		li $v0, 4			
		la $a0, msgRec		
		syscall
		li $v0, 1			
		move $a0, $t3		
		syscall
	
		move $ra, $s0		# $ra <- $s0, restore the return address after the procedure
		jr $ra			# jump to exit

warn:		li $v0, 4			# $v0 <- 4, load immediate value 4 for print a string
		la $a0, warning		# $a0 <- address of the result string
		syscall
		j read			# jump to label read

Ackermann:	addi $t3, $t3, 1
		addi $sp, $sp, -4
		sw $ra, 4($sp)
		beq $t0, $0, Ackermann1
		addi $sp, $sp, -4
		sw $t0, 4($sp)

		jal Ackermann2

		lw $t0, 4($sp) 		
		lw $ra, 8($sp) 		
		addu $sp, $sp, 8 		
		jr $ra

Ackermann3:	addi $sp, $sp, -4
		sw $ra, 4($sp)
		addi $sp, $sp, -4
		sw $t0, 4($sp)

		addi $t1, $t1, -1
		jal Ackermann		
		addi $t0, $t0, -1
		jal Ackermann

		lw $t0, 4($sp) 		
		lw $ra, 8($sp) 		
		addu $sp, $sp, 8
		jr $ra 		

Ackermann2:	bne $t1, $0, Ackermann3
		addi $sp, $sp, -4
		sw $ra, 4($sp)
		addi $sp, $sp, -4
		sw $t0, 4($sp)
		addi $t0, $t0, -1
		addi $t1, $0, 1
		jal Ackermann

		lw $t0, 4($sp) 		
		lw $ra, 8($sp) 		
		addu $sp, $sp, 8
		jr $ra 	

Ackermann1:	addi $t1, $t1, 1		# $t1 <- $t1 + 1,  add $t1 with immediate value 1
		lw $ra, 4($sp) 		# get the return address
		addu $sp, $sp, 4 		# adjust the stack pointer
		jr $ra 			# return