#include <stdio.h>
#include <stdlib.h>

int Ackermann(int x, int y);

int main()
{
   int x, y;

   printf( "Please enter a value for x and y :");
    scanf("%d %d", &x, &y);

   int result;
   result = Ackermann(x, y);

    printf( "The value is : %d", result);

   return 0;
}

int Ackermann(int x, int y)
{
   if (x == 0)
   {
        return y+1;
   }
   else if (y == 0)
   {
       return Ackermann(x-1, 1);
   }
   else
   {
       return Ackermann(x-1, Ackermann(x, y-1));
   }
}
