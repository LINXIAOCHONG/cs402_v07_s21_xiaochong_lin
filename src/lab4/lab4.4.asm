	.data
largemsg:	.asciiz "the largest value is:\n"	#a message for the procedure largest
	.text
	.globl main
main: 	move $s0, $ra 	# must save $ra since I’ll have a call

	add $sp, $sp, -8	# $sp <- $sp - 8, stack pointer move two words down
	li $v0, 5		# $v0 <- 5, load immediate value 5 to $v0
	syscall
	sw $v0, 8($sp)	# 8($sp) <- $v0, copy the read value to current stack pointer with offset 2 words
	li $v0, 5		# $v0 <- 5, load immediate value 5 to $v0
	syscall
	sw $v0, 4($sp) 	# 4($sp) <- $v0, copy the read value to current stack pointer with offset 1 word

	jal largest 		# call ‘largest’

	nop 		# execute this after ‘largest’ returns

	move $ra, $s0 	# restore the return address in $ra
	jr $ra 		# return from main
largest: 	
	li $v0, 4		# $v0 <- 4, load immediate value 4 to $v0
	la $a0, largemsg	# $a0 <- address of the string largemsg
	syscall

	lw $t0, 8($sp)	# $t0 <- 8($sp), load word from stack with offset of 2 words from current stack pointer
	lw $t1, 4($sp)	# $t1 <- 4($sp), load word from stack with offset of 1 word from current stack pointer

	bgt $t0, $t1, bigger	# jump to branch labeled‘bigger’

	li $v0, 1		# $v0 <- 1, load immediate value 1 to $v0
	move $a0, $t1	# $a0 <- $t1, move bigger value in $t1 to $a0
	syscall

	j exitif		# jump to branch exitif
bigger:	
	li $v0, 1		# $v0 <- 1, load immediate value 1 to $v0
	move $a0, $t0	# $a0 <- $t0, move bigger value in $t0 to $a0
	syscall
exitif:
	jr $ra 		# return from this procedure