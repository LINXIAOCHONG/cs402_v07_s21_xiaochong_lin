	.data
largemsg:	.asciiz "the largest value is:\n"	#a message for the procedure largest
	.text
	.globl main
main: 	move $s0, $ra 	# must save $ra since I’ll have a call

	li $v0, 5		# $v0 <- 5, load immediate value 5 to $v0
	syscall
	move $t0, $v0	# $t0 <- $v0, copy the read value to $t0
	li $v0, 5		# $v0 <- 5, load immediate value 5 to $v0
	syscall
	move $t1, $v0	# $t1 <- $v0, copy the read value to $t1

	jal largest 		# call ‘largest’

	nop 		# execute this after ‘largest’ returns

	move $ra, $s0 	# restore the return address in $ra
	jr $ra 		# return from main
largest: 	
	li $v0, 4		# $v0 <- 4, load immediate value 4 to $v0
	la $a0, largemsg	# $a0 <- address of the string largemsg
	syscall

	bgt $t0, $t1, bigger	# jump to branch labeled‘bigger’

	li $v0, 1		# $v0 <- 1, load immediate value 1 to $v0
	move $a0, $t1	# $a0 <- $t1, move bigger value in $t1 to $a0
	syscall

	j exitif		# jump to branch exitif
bigger:	
	li $v0, 1		# $v0 <- 1, load immediate value 1 to $v0
	move $a0, $t0	# $a0 <- $t0, move bigger value in $t0 to $a0
	syscall
exitif:
	jr $ra 		# return from this procedure