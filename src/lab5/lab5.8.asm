		.data
word1:		.word 0x89abcdef
		.text
		.globl main
main: 		add $sp, $sp, -4	# $sp <- $sp -4, $sp move one word lower
		sw $ra, 4($sp) 	# $sp + 4 <- $ra, store the $ra into stack

		lwr $t4, word1+0
		lwr $t5, word1+1
		lwr $t6, word1+2
		lwr $t7, word1+3

		lw $ra, 4($sp) 	# $ra <- $sp + 4, restore the return address from $sp + 0 to $ra
		addu $sp, $sp, 4	# $sp <- $sp + 4, stack pop, move one word higher
		jr $ra 		# return from main