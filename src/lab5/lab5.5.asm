# this is a program used to test memory alignment for data
		.data 0x10000000
char1: 		.byte 'a' 		# reserve space for a byte
double1: 		.double 1.1 	# reserve space for a double
char2: 		.byte 'b' 		# b is 0x62 in ASCII
half1: 		.half 0x8001 	# reserve space for a half-word (2 bytes)
char3: 		.byte 'c' 		# c is 0x63 in ASCII
word1: 		.word 0x56789abc 	# reserve space for a word
char4: 		.byte 'd' 		# d is 0x64 in ASCII
word2:		.word 0x00000000	# reserve space for a word with value 0
		.text
		.globl main
main:		addu $s0, $ra, $0	# $s0 <- $ra, store return address to $s0
		
		lb $t0, word1+3	# load the first byte of word1 to $t0
		lb $t1, word1+2	# load the second byte of word1 to $t1
		lb $t2, word1+1	# load the third byte of word1 to $t2
		lb $t3, word1+0	# load the forth byte of word 1 to $t3
		lbu $t4, word1+3	# load the first byte of word1 to $t4
		lbu $t5, word1+2	# load the second byte of word1 to $t5
		lbu $t6, word1+1	# load the third byte of word1 to $t6
		lbu $t7, word1+0	# load the forth byte of word 1 to $t7
		lh $t8, half1	# load the half1 to $t8
		lhu $t9, half1	# load the half1 to $t9

		sb $t3, word2+3
		sb $t2, word2+2
		sb $t1, word2+1
		sb $t0, word2+0

		addu $ra, $s0, $0	# $ra <- $s0, restore return address to $ra 
		jr $ra 		# return from main