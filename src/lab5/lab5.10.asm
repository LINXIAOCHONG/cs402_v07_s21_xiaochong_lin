		.data
		.align 0
ch1: 		.byte 'a'
word1: 		.word 0x89abcdef
ch2: 		.byte 'b'
word2:		.word 0x00000000
		.text
		.globl main
main:		addu $s0, $ra, $0
		
		lui $at, 0x1000
		ori $t0, $at, 0x0000

		lwr $t1, 1($t0)
		lwl $t1, 4($t0)

		swr $t1, 6($t0)
		swl $t1, 9($t0)		

		addu $ra, $s0, $0
		jr $ra 		# return from main
		