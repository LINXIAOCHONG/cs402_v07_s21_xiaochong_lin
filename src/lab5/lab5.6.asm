		.data
msg:		.asciiz "Please enter an integer:\n"
msg2:		.asciiz "If bytes were layed in reverse order the number would be:\n"
user1:		.word 0x00000000
user2:		.word 0x00000000
		.text
		.globl main
main: 		add $sp, $sp, -4	# $sp <- $sp -4, $sp move one word lower
		sw $ra, 4($sp) 	# $sp + 4 <- $ra, store the $ra into stack

		li $v0, 4		# $v0 <- 4, load immediate value 4 for print a string
		la $a0, msg	# $a0 <- address of the string
		syscall
read:		li $v0, 5		# $v0 <- 5, load immediate value 5 for read int
		syscall
		sw $v0, user1

		lbu $t0, user1+3
		lbu $t1, user1+2
		lbu $t2, user1+1
		lbu $t3, user1+0

		sb $t3, user2+3
		sb $t2, user2+2
		sb $t1, user2+1
		sb $t0, user2+0

		lw $t4, user2

		jal Reverse_bytes 	

		lw $ra, 4($sp) 	# $ra <- $sp + 4, restore the return address from $sp + 0 to $ra
		addu $sp, $sp, 4	# $sp <- $sp + 4, stack pop, move one word higher
		jr $ra 		# return from main

Reverse_bytes: 	
		li $v0, 4		# $v0 <- 4, load immediate value 4 for print a string
		la $a0, msg2	# $a0 <- address of the string
		syscall
		li $v0, 1		# $v0 <- 4, load immediate value 4 for print a integer
		move $a0, $t4	# $a0 <- copy $v0 to $a0
		syscall
		jr $ra 		# return from this procedure