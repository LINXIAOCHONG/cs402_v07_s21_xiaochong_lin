	.data 0x10000000
var1:	.word 	0x5		#declare word size for var1 initial value 5
var2:	.word	0x8		#declare word size for var2 initial value 8
var3:	.word	0xFFFFFFF81B	#declare word size for var3 initial value -2021
	.text
	.globl main
main:	addu $s0, $ra, $0		#save return address $ra to $s0
	lw $t1, var1		#load var1 to $t1
	lw $t2, var2		#load var2 to $t2
	lw $t3, var3		#load var3 to $t3
if:	slt $t9, $t1, $t2		#if $t1 < $t2, $t9=1, else $t9=0
	bgtz $t9, else		#if $t9 > 0 then jump to else
	move $t1, $t3		#copy $t3 to $t1
	move $t2, $t3		#copy $t3 to $t1
	j endif			#jump label endif
else:	move $t9, $t1		#copy $t1 to $t9
	move $t1, $t2		#copy $t2 to $t1
	move $t2, $t9		#copy $t9 to $t2
endif:	sw $t1 var1		#store var1 from $t1
	sw $t2 var2		#store var2 from $t2
	addu $ra, $0, $s0		#restore return address to $ra
	jr $ra			#return from main


		