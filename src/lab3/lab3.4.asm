	.data 0x10000000
my_array:	.space 	40		#reserved space for an array at word size 10 named my_array
	.text
	.globl main
main:	addu $s0, $ra, $0		#$s0<-$ra, save return address $ra to $s0
	li $t1, 5			#$t1<-5, use $t1 as j and initial the value with 5
	li $t2, 10			#$t2<-10, use $t2 as for limit, set the value as 10
	li $t3, 0			#$t3 <-0, use $t3 as i and initial the value with 0
	la $t0, my_array		#$t0<-the address of my_array at position i
#start loop
loop:	ble $t2, $t3, endfor		#if ($t2(limit) <= $t3(i)) jump to lable endfor
	sw $t1, 0($t0)		#$t1<-my_array[i], store the value to the my_array at position i
	addi $t0, $t0, 4		#$t0<-$t0+4, set the $t0 point to the next my_array address
	addi $t1, $t1, 1		#$t1<-$t1+1, increase $t1(j) by 1
	addi $t3, $t3, 1		#$t3<-$t3+1, increase $t3(i) by 1
	j loop			#jump loop
#end loop
endfor:	
	addu $ra, $0, $s0		#restore return address to $ra
	jr $ra			#return from main


		