	.data 0x10000000
msg_far:	.asciiz	"I'm far away"
msg_near:	.asciiz	"I'm nearby"
	.text
	.globl main
main:	addu $s0, $ra, $0		#$s0<-$ra, save return address $ra to $s0
	li $v0, 5			#$v0<-5, prompts the user to enter an integers
	syscall
	move $t0, $v0		#$t0<-$v0, store the input value to tempoary register 0
	li $v0, 5			#$v0<-5,  prompts the user to enter an integers
	syscall
	move $t1, $v0		#$t1<-$v0, store the input value to tempoary register 0
if:	
	beq $t0, $t1, far		#if ($t0=$t1) jump to label far
	li $v0, 4			#$v0<-4, print a message
	la $a0, msg_near		#$a0<-address of label msg_near
	syscall
	j endif			#jump endif
far:	li $v0, 4			#$v0<-4, print a message
	la $a0, msg_far		#$a0<-address of label msg_near
	syscall
endif:
	addu $ra, $0, $s0		#$ra<-$s0, restore return address to $ra
	jr $ra			#return from main


		