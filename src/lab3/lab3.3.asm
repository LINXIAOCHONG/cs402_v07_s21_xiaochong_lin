	.data 0x10000000
var1:	.word 	0x5		#declare word size for var1 initial value 5
	.text
	.globl main
main:	addu $s0, $ra, $0		#save return address $ra to $s0
	lw $t1, var1		#load var1 to $t1
	move $a0, $t1		#copy $a0 to $t0 as i
	li $a1, 100			#load immediate value 100 to $a1 as limit
#start loop
loop:	ble $a1, $a0, endfor		#exit if $a1(limit) <= $a0(i)
	addi $t1, $t1, 1		#add $t1(var1) with 1
	addi $a0, $a0, 1		#add $a0(i) with 1
	j loop			#jump loop
endfor:	sw $t1 var1		#store var1 from $t1
#end loop
	addu $ra, $0, $s0		#restore return address to $ra
	jr $ra			#return from main


		