#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "basicstats_function.h"

void PrintContent(float inputValue[], int size){
    for (int i=0; i<size; i++){
        printf("%f\n", inputValue[i]);
    }
}

void LoadContent(FILE *fp, float **inputValueAddress, int *size, int *arraySize){
    float *inputValue = *inputValueAddress;                                                 //copy the array address
    int i=0, times=1, aSize = *arraySize;                                                   //initial helper value
    float *newArray=NULL;                                                                   //initial helper array pointer which to be work as a intermediate array
    char buff[MAXSTRING];                                                                   //initial helper string to store each line of the file
    while (!feof(fp)){                                                                      //read the file line by line
        if (aSize < i+1){                                                                   //if the array size is not enough to store the value, expand the array
            aSize = ARRAYSIZE * pow(2, times++);                                            //double the size of array
            newArray = (float*)malloc(aSize*sizeof(float));                               //allocate memory to helper array
            TransferData(inputValue, newArray, i);                                          //transfer data from old array to new array
            free(inputValue);                                                               //release the memory of old array
            inputValue = newArray;                                                          //set the pointer to new array
            newArray = NULL;                                                                //set the helper array to point to NULL to release the memory
        }
        fscanf(fp, "%s", buff);                                                             //read each line from the file
        inputValue[i] = atof(buff);                                                       //store each line value to the array
        i++;                                                                                //move on
    }
    *size = i;                                                                              //return the size of values
    *arraySize = aSize;                                                                     //return the size of array allocated memory
    *inputValueAddress = inputValue;                                                        //return the array address
}

void TransferData(float currentArray[], float newArray[], int size){
    for (int i=0; i<size; i++){
        newArray[i] = currentArray[i];
    }
}

void GetMean(float inputValue[], int size, float *mean){
    float tmpFloat=0;
    for (int i=0; i<size; i++){
        tmpFloat += inputValue[i];
    }
    *mean = tmpFloat / size;
}

void sortNumber(float inputValue[], int size){
    qsort(inputValue, size, sizeof(float), compare);
}

int compare(float *value1, float *value2){
    return *value1>*value2?1:-1;
}

void GetMedian(float *inputValue, int size, float *median){
    sortNumber(inputValue, size);
    int m=size/2;
    if (fmod(size, 2)==0){                                                                  //check if the values number is even or not
        *median = (inputValue[m]+inputValue[m-1])/2;                                        //if it is even, the median is the average of middle two value
    }
    else{
        *median = inputValue[m-1];                                                          //if it is odd, the median is the middle value
    }
}

void GetStddve(float inputValue[], int size, float *stddev, float mean){
    float sumFloat=0;
    for (int i=0; i<size; i++){
        sumFloat += pow(inputValue[i]-mean, 2);
    }
    *stddev = sqrt(sumFloat/size);
}
