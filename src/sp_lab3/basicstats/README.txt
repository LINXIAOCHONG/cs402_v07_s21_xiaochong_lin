For home work SP_LAB_03, the submission of the files are as the below list:
1.Please enter src/sp_lab3/basicstats.
2.The files for homework is:
    a. basicstats_function.c
    b. basicstats_function.h
    c. main.c
    d. readfile.c
    e. readfile.h
    f. small.txt
    g. large.txt
3.Import these files to the workspace and compile them to execute. Please set the argument to be the data filename.
4.Or copy the executable file locates at bin\release. Please copy the data file to the same directory of executable file. Entering the command line: basicstats [data filename] to run this programme.
5.The description of the function in this programme

	void PrintContent(float *inputValue, int size);
	//print the list of the input data

	void LoadContent(FILE *fp, float **inputValueAddress, int *size, int *arraySize);
	//load the data into the array

	void GetMean(float inputValue[], int size, float *mean);
	//calculate the mean of the data

	void GetMedian(float inputValue[], int size, float *median);
	//compute the median of the data

	void GetStddve(float inputValue[], int size, float *stddev, float mean);
	//compute the stddev of the data

	int compare(float *value1, float *value2);
	//compare function for the sorting
	