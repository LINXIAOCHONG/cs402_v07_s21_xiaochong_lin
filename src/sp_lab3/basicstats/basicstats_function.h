#ifndef BASICSTATS_FUNCTION_H_INCLUDED
#define BASICSTATS_FUNCTION_H_INCLUDED
#define MAXSTRING 255
#define ARRAYSIZE 20

void PrintContent(float *inputValue, int size);
void LoadContent(FILE *fp, float **inputValueAddress, int *size, int *arraySize);
void GetMean(float inputValue[], int size, float *mean);
void GetMedian(float inputValue[], int size, float *median);
void GetStddve(float inputValue[], int size, float *stddev, float mean);
int compare(float *value1, float *value2);

#endif // BASICSTATS_FUNCTION_H_INCLUDED
