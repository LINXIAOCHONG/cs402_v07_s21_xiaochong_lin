#include <stdio.h>
#include <stdlib.h>
#include "readfile.h"
#include "basicstats_function.h"

int main(int argc, char **argv)
{
    char *fileName = argv[1];                                           //read argument from the command line
    int size=0, arraySize = ARRAYSIZE;                                  //initial integer variable
    float mean=0, median=0, stddve=0;                                   //initial float variable
    float *inputValue = (float*)malloc(arraySize*sizeof(float));      //initial array at size of ARRAYSIZE=20
    float **inputValueAddress = &inputValue;                            //initial array address point to array

    FILE *fp=NULL;                                                       //initial FILE pointer

    if (open_file(&fp, fileName) != 0){                                 //open file from the input argument
        printf("file does not exist!");
    }
    else{
        LoadContent(fp, inputValueAddress, &size, &arraySize);          //load the file into array

        inputValue = *inputValueAddress;                                //pass the address to array pointer

        //PrintContent(inputValue, size);

        GetMean(inputValue, size, &mean);                               //get the value of mean
        GetMedian(inputValue, size, &median);                           //get the value of median
        GetStddve(inputValue, size, &stddve, mean);                     //get the value of stddve

        printf("Results:\n");                                           //print the result
        printf("--------\n");
        printf("Num values:%16d\n", size);
        printf("      mean:%16.3f\n", mean);
        printf("    median:%16.3f\n", median);
        printf("    stddev:%16.3f\n", stddve);
        printf("Unused array capacity:%d", arraySize-size);
    }

    close_file(fp);                                                     //close file
    return 0;
}
