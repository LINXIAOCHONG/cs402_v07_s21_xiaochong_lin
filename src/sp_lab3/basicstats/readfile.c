#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "readfile.h"

int open_file(FILE **fp, char fn[])
{
    *fp = fopen(fn, "r");

    if (fp != NULL)
        return 0;
    else
        return -1;
}
int read_int(int *ri)
{
    if(scanf("%d", ri) == 0)
        return -1;
    return 0;
}
char read_string(char rs[])
{
    if(scanf("%s", rs) == 0)
        return -1;
    return 0;
}
float read_float(float *rf)
{
    if(scanf("%f", rf) == 0)
        return -1;
    return 0;
}
void close_file(FILE *fp)
{
    fclose(fp);
}
