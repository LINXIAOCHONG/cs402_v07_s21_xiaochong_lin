package hw01;

public class myMatrix {
	private int row = 100;
	private int col = 50;
	private int[][] leftInt, rightInt, resultInt;
	private double[][] leftDouble, rightDouble, resultDouble;
	private boolean initializeMatrix = false;
	private long startTimeInt, endTimeInt, startTimeDouble, endTimeDouble;
	private int timeConsumeInt, timeConsumeDouble, timeConsumeInt2, timeConsumeDouble2;
	
	public void setRow(int r){row = r;}
	public void setCol(int c){col = c;}
	public int[][] getResultInt(){return resultInt;}
	public double[][] getResultDouble(){return resultDouble;}
	public int getTimeConsumeInt() {return timeConsumeInt;}
	public int getTimeConsumeDouble() {return timeConsumeDouble;}
	public int getTimeConsumeInt2() {return timeConsumeInt2;}
	public int getTimeConsumeDouble2() {return timeConsumeDouble2;}
	
	protected void generateRandomMatrix() {
		int max = row >= col? row : col;

		leftInt = new int[row][col];
		rightInt = new int[col][row];
		resultInt = new int[row][row];
		leftDouble =  new double[row][col];
		rightDouble =  new double[col][row];
		resultDouble = new double[row][row];
		
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				leftInt[i][j] = (int) (Math.random() * max + 1);
				rightInt[j][i] = (int) (Math.random() * max + 1);
				leftDouble[i][j] = Math.random();
				rightDouble[j][i] = Math.random();
			}
		}
		initializeMatrix = true;
	}
	
	public void printMatrix() {
		if (!initializeMatrix) {
			this.generateRandomMatrix();
		}
		for (int i = 0; i < leftInt.length; i++) {
			for (int j = 0; j < leftInt[0].length; j++) {
				System.out.print(leftInt[i][j] + ",");
			}
			System.out.println();
		}
	}
	
	public void printMatrixResult() {
		this.multiplyMatrix();
		for (int i = 0; i < resultInt.length; i++) {
			for (int j = 0; j < resultInt[0].length; j++) {
				System.out.print(resultInt[i][j] + ",");
			}
			System.out.println();
		}
	}
	
	public void multiplyMatrix() {
		if (!initializeMatrix) {
			this.generateRandomMatrix();
		}
		startTimeInt = System.currentTimeMillis();
		for (int i = 0; i < leftInt.length; i++) {
			for (int j = 0; j < rightInt[0].length; j++) {
				for (int k = 0; k < leftInt[0].length; k++) {
					resultInt[i][j] += leftInt[i][k] * rightInt[k][j];					
				}
			}
		}
		endTimeInt = System.currentTimeMillis();
		timeConsumeInt = (int) (endTimeInt - startTimeInt);
		startTimeDouble = System.currentTimeMillis();
		for (int i = 0; i < leftDouble.length; i++) {
			for (int j = 0; j < rightDouble[0].length; j++) {
				for (int k = 0; k < leftDouble[0].length; k++) {
					resultDouble[i][j] += leftDouble[i][k] * rightDouble[k][j];					
				}
			}
		}
		endTimeDouble = System.currentTimeMillis();
		timeConsumeDouble = (int) (endTimeDouble - startTimeDouble);
	}
	
	public void multiplyMatrix2() {
		if (!initializeMatrix) {
			this.generateRandomMatrix();
		}
		startTimeInt = System.currentTimeMillis();
		for (int j = 0; j < rightInt[0].length; j++) {
			for (int i = 0; i < leftInt.length; i++) {
				for (int k = 0; k < leftInt[0].length; k++) {
					resultInt[i][j] += leftInt[i][k] * rightInt[k][j];					
				}
			}
		}
		endTimeInt = System.currentTimeMillis();
		timeConsumeInt2 = (int) (endTimeInt - startTimeInt);
		startTimeDouble = System.currentTimeMillis();
		for (int j = 0; j < rightDouble[0].length; j++) {
			for (int i = 0; i < leftDouble.length; i++) {
				for (int k = 0; k < leftDouble[0].length; k++) {
					resultDouble[i][j] += leftDouble[i][k] * rightDouble[k][j];					
				}
			}
		}
		endTimeDouble = System.currentTimeMillis();
		timeConsumeDouble2 = (int) (endTimeDouble - startTimeDouble);
	}
}