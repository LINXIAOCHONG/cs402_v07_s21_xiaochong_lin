package hw01;

import java.util.Scanner;

public class HW01_02 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int row, col;
		myMatrix matrix = new myMatrix();
		
		Scanner scan = new Scanner(System.in);

		System.out.println("Please enter row number, 0 for default:");
		row = scan.nextInt();
		System.out.println("Please enter col number, 0 for default:");
		col = scan.nextInt();
		
		if (row != 0) matrix.setRow(row);
		if (col != 0) matrix.setCol(col);
		
		//metrix.printMetrix();
		//metrix.printMetrixResult();
		matrix.multiplyMatrix();
		matrix.multiplyMatrix2();
		
		System.out.println("Matrix Int takes:" + matrix.getTimeConsumeInt() + " milliseconds");
		System.out.println("Matrix Double takes:" + matrix.getTimeConsumeDouble() + " milliseconds");
		System.out.println("Matrix Int2 takes:" + matrix.getTimeConsumeInt2() + " milliseconds");
		System.out.println("Matrix Double2 takes:" + matrix.getTimeConsumeDouble2() + " milliseconds");
		
		scan.close();
	}

}