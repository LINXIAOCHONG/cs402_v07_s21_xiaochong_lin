#ifndef READFILE_H_INCLUDED
#define READFILE_H_INCLUDED

int open_file(FILE *fp, char fn[]);
int read_int(int *ri);
char read_string(char rs[255]);
float read_float(float *rf);
void close_file(FILE *fp);


#endif // READFILE_H_INCLUDED
