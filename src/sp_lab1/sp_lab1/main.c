#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "employee.h"
#include "readfile.h"

int main()
{
    enum Selection selection = Quit;

    do
    {
        printf("Employ DB Menu:\n");
        printf("------------------------------------\n");
        printf("1.	Print the Database\n");
        printf("2.	Lookup employee by ID\n");
        printf("3.	Lookup employee by last name\n");
        printf("4.	Add an Employee\n");
        printf("5.	Quit\n");
        printf("6.	Remove an employee\n");
        printf("7.	Update an employee's information\n");
        printf("8.	Print the M employees with the highest salaries\n");
        printf("9.	Find all employees with matching last name\n");
        printf("------------------------------------\n");

        printf("Please enter your selection: ");
        scanf("%d", &selection);

        if ((selection <= 9) & (selection >= 1)){
            char fn[32]="small.txt";

            FILE *fp = fopen(fn, "r+");

            struct Employee employees[MAXARR];
            int employeeCount = 0;

            if (open_file(fp, fn) != 0){
                printf("file does not exist!");
            }
            else{
                loadEmployees(employees, fp, &employeeCount);
            }

            struct Employee target;
            int six_digit_ID, salary, confirm, new_ID, highest;
            char first_name[MAXNAME], last_name[MAXNAME];

            switch (selection){
            case Print:
                sortEmployee(employees, employeeCount, 1);
                printEmployees(employees, employeeCount);
                break;
            case LookupByID:
                printf("Enter a 6 digit employee id: ");
                read_int(&six_digit_ID);
                target.six_digit_ID = six_digit_ID;
                sortEmployee(employees, employeeCount, 1);
                binarySearchEmployee(employees, employeeCount, &target, 1, LookupByID);
                break;
            case LookupByLastName:
                printf("Enter Employee's last name (no extra spaces): ");
                read_string(last_name);
                strcpy(target.last_name, last_name);
                sortEmployee(employees, employeeCount, 3);
                binarySearchEmployee(employees, employeeCount, &target, 3, LookupByLastName);
                break;
            case Add:
                printf("Enter the first name of the employee: ");
                read_string(first_name);
                strcpy(target.first_name, first_name);
                printf("Enter the last name of the employee: ");
                read_string(last_name);
                strcpy(target.last_name, last_name);
                printf("Enter employee's salary (30000 to 150000): ");
                read_int(&salary);

                printf("do you want to add the following employee to the DB?\n");
                printf("%s, %s, salary: %d\n", first_name, last_name, salary);
                printf("Enter 1 for yes, 0 for no: ");
                read_int(&confirm);

                if (confirm == 1){
                    sortEmployee(employees, employeeCount, 1);
                    six_digit_ID = employees[employeeCount-1].six_digit_ID;
                    addEmployee(employees, fp, six_digit_ID+1, first_name, last_name, salary, employeeCount);
                    printEmployees(employees, employeeCount+1);
                }
                break;
            case Quit:
                break;
            case Remove:
                printf("Enter a 6 digit employee id: ");
                read_int(&six_digit_ID);
                target.six_digit_ID = six_digit_ID;
                sortEmployee(employees, employeeCount, 1);
                binarySearchEmployee(employees, employeeCount, &target, 1, Remove);

                FILE *fs = fopen(fn, "w");
                saveEmployee(employees, fs, employeeCount-1);
                close_file(fs);
                break;
            case Update:
                printf("enter a new ID:");
                read_int(&new_ID);

                while (new_ID < 100000 || new_ID > 999999){
                    printf("ID number out of range,please try again!\n");
                    printf("enter a new ID:");
                    read_int(&new_ID);
                }
                target.six_digit_ID = new_ID;
                sortEmployee(employees, employeeCount, 1);
                binarySearchEmployee(employees, employeeCount, &target, 1, Update);
                break;
            case HighestSalaries:
                printf("Please enter highest number:");
                read_int(&highest);

                while (highest < 0 || highest > employeeCount){
                    printf("The input number is out of range,please try again!\n");
                    printf("Please enter highest number:");
                    read_int(&highest);
                }

                sortEmployee(employees, employeeCount, 4);
                printEmployees(employees, highest);

                break;
            case FindAll:
                printf("Please enter employee's last name (no extra spaces): ");
                read_string(last_name);
                strcpy(target.last_name, last_name);
                binarySearchEmployee(employees, employeeCount, &target, 3, FindAll);

                break;
            default:
                printf("Hey, %d is not between 1 to 9, please make your choice again.\n", selection);
            }
            close_file(fp);
        }
        else{
            printf("Hey, %d is not between 1 to 9, please make your choice again.\n", selection);
        }
    }while(selection != 5);
    printf("goodbye!\n");
    return 0;
}
