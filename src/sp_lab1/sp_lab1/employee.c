#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "employee.h"

void loadEmployees(struct Employee *employees, FILE* fp, int *ec){
    char buff[255] = "";
    int i = 0;
    while ((!feof(fp)) && (i < MAXARR))
    {
        fgets(buff, 255, fp);
        if (strlen(buff) == 0){
            break;
        }
        buildEmployee(&employees[i], buff);
        strcpy(buff, "");
        i++;
    };
    *ec=i;
}

void buildEmployee(struct Employee *employee, char buff[]){
    int switchCount = 0;
    int i = 0;
    int start = 0;
    int len = 0;
    char dst[32];
    char fn[32];
    char ln[32];
    while (buff[i] != '\n'){
        if (buff[i] == 32){
            if (buff[i+1] != 32){
                switch (switchCount){
                case 0:
                    substr(dst, buff, start, len);
                    employee->six_digit_ID = atoi(dst);
                    break;
                case 1:
                    substr(fn, buff, start, len);
                    strcpy(employee->first_name, fn);
                    break;
                case 2:
                    substr(ln, buff, start, len);
                    strcpy(employee->last_name, ln);
                    break;
                case 3:
                    substr(dst, buff, start, len);
                    employee->salary = atoi(dst);
                    break;
                }
                switchCount++;
                start = i + 1;
                len = 0;
            }
            else{
                len++;
            }
        }
        else{
            len++;
        }
        i++;
    }
    substr(dst, buff, start, len);
    employee->salary = atoi(dst);
}

int substr(char dst[],char src[],int start,int len){
	char* p = src + start;
	int n = strlen(p);
	int i = 0;
		if(n < len)
		{
			len = n;
		}
		while(len != 0)
		{
			dst[i] = src[i+start];
			len --;
			i++;
		}
		dst[i] = '\0';
	return 0;
}

void addEmployee(struct Employee employees[], FILE* fp, int six_digit_ID, char first_name[], char last_name[], int salary, int index){
    employees[index].six_digit_ID = six_digit_ID;
    strcpy(employees[index].first_name, first_name);
    strcpy(employees[index].last_name, last_name);
    employees[index].salary = salary;
    fprintf(fp,"%d %s %s %d\n",six_digit_ID, first_name, last_name, salary);
}

void saveEmployee(struct Employee employees[], FILE* fp, int count){
    for (int i=0; i<count; i++){
        fprintf(fp,"%d %s %s %d\n", employees[i].six_digit_ID, employees[i].first_name, employees[i].last_name, employees[i].salary);
    }
}

void printEmployees(struct Employee *employees, int count){
    printf("\n");
    printf("%-15s", "NAME");
    printf("%-15s", " ");
    printf("%-15s", "SALARY");
    printf("%-15s", "ID");
    printf("\n");
    printf("----------------------------------------------------------------\n");
    for (int i = 0; i < count; i++){
        printf("%-15s", employees[i].first_name);
        printf("%-15s", employees[i].last_name);
        printf("%-15d", employees[i].salary);
        printf("%-15d", employees[i].six_digit_ID);
        printf("\n");
    }
    printf("----------------------------------------------------------------\n");
    printf("Number of Employees( %d )\n", count);
    printf("\n");
}

void printEmployee(struct Employee *employee){
    printf("\n");
    printf("%-15s", "NAME");
    printf("%-15s", " ");
    printf("%-15s", "SALARY");
    printf("%-15s", "ID");
    printf("\n");
    printf("----------------------------------------------------------------\n");

        printf("%-15s", employee->first_name);
        printf("%-15s", employee->last_name);
        printf("%-15d", employee->salary);
        printf("%-15d", employee->six_digit_ID);
        printf("\n");

    printf("----------------------------------------------------------------\n");
    printf("\n");
}

void updateEmployee(struct Employee employees[], struct Employee *employee, int employeeCount){
    int newID, newSalary, confirm, repeat;
    char first_name[MAXNAME], last_name[MAXNAME];
    struct Employee target, *result;

    printf("You will update %s information!\n", employee->last_name);

    printf("Please enter a new ID:");
    read_int(&newID);

    repeat = 1;
    while (repeat){
        repeat = 0;
        if (newID < 100000 || newID > 999999){
            printf("ID number out of range,please try again!\n");
            printf("Please enter a new ID:");
            read_int(&newID);
            repeat = 1;
        }
        else {
            target.six_digit_ID = newID;
            result = (struct Employee*)bsearch (&target, employees, employeeCount, sizeof(struct Employee) , bsCompare);
            if (result != NULL){
                printf("ID number already exist,please try again!\n");
                printf("Please enter a new ID:");
                read_int(&newID);
                repeat = 1;
            }
        }
    }

    printf("\n");
    printf("Please enter first name:");
    read_string(first_name);
    printf("\n");
    printf("Please enter last name:");
    read_string(last_name);
    printf("\n");

    printf("Please enter a new salary:");
    read_int(&newSalary);

    while (newSalary < 30000 || newSalary > 150000){
        printf("Salary is out of range,please try again!\n");
        printf("Please enter a new salary:");
        read_int(&newSalary);
    }
    printf("\n");

    printf("Please confirm to update the employee new information!\n");
    printf("Enter 1 for yes, 0 for no: ");
    read_int(&confirm);
    printf("\n");

    if (confirm == 1){
        employee->six_digit_ID = newID;
        strcpy(employee->first_name, first_name);
        strcpy(employee->last_name, last_name);
        employee->salary = newSalary;

        FILE *fs = fopen("small.txt", "w");
        saveEmployee(employees, fs, employeeCount);
        close_file(fs);

        printf("Employee updated successfully!\n");
    }
}

int compare(struct Employee *employee1, struct Employee *employee2){
    int ID1= employee1->six_digit_ID;
    int ID2= employee2->six_digit_ID;
    return ID1>ID2?1:-1;
}

int compareFirstName(struct Employee *employee1, struct Employee *employee2){
    char fn1[MAXNAME], fn2[MAXNAME];
    strcpy(fn1, employee1->first_name);
    strcpy(fn2, employee2->first_name);
    return strcmp(fn1, fn2)>0?1:-1;
}

int compareLastName(struct Employee *employee1, struct Employee *employee2){
    char ln1[MAXNAME], ln2[MAXNAME];
    strcpy(ln1, employee1->last_name);
    strcpy(ln2, employee2->last_name);
    return strcmp(ln1, ln2)>0?1:-1;
}

int bsCompare(struct Employee *employee1, struct Employee *employee2){
    int ID1= employee1->six_digit_ID;
    int ID2= employee2->six_digit_ID;
    if (ID1==ID2)
        return 0;
    else if (ID1 > ID2)
        return 1;
    else
        return -1;
}

int bsCompareLastName(struct Employee *employee1, struct Employee *employee2){
    char ln1[MAXNAME], ln2[MAXNAME];
    strcpy(ln1, employee1->last_name);
    strcpy(ln2, employee2->last_name);
    if (strcmp(ln1, ln2)==0)
        return 0;
    else if (strcmp(ln1, ln2)>0)
        return 1;
    else
        return -1;
}

void sortEmployee(struct Employee employees[], int count, enum EmployeeField field){
    switch (field){
    case six_digit_ID:
        qsort(employees, count, sizeof(struct Employee), compare);
        break;
    case first_name:
        qsort(employees, count, sizeof(struct Employee), compareFirstName);
        break;
    case last_name:
        qsort(employees, count, sizeof(struct Employee), compareLastName);
        break;
    case salary:
        qsort(employees, count, sizeof(struct Employee), compare);
        break;
    }
}

void binarySearchEmployee(struct Employee employees[], int count, struct Employee *target, enum EmployeeField field, enum Selection selection){
    struct Employee* result;
    int confirm=0, found=0;
    switch (field){
    case six_digit_ID:
        result = (struct Employee*)bsearch (target, employees, count, sizeof(struct Employee) , bsCompare);
        if (result == NULL){
            printf("Employee with id %d not found in DB\n", target->six_digit_ID);
            printf("\n");
        }
        else{
            switch (selection){
            case LookupByID:
                printEmployee(result);
                break;
            case Remove:
                printf("Do you want to remove the employee id %d in DB?\n", target->six_digit_ID);
                printf("Enter 1 for yes, 0 for no: ");
                read_int(&confirm);
                if (confirm == 1){
                    result->six_digit_ID = employees[count-1].six_digit_ID;
                    strcpy(result->first_name, employees[count-1].first_name);
                    strcpy(result->last_name, employees[count-1].last_name);
                    result->salary = employees[count-1].salary;
                }
                break;
            case Update:
                updateEmployee(employees, result, count);
                break;
            }
        }
        break;
    case last_name:
        switch (selection){
        case LookupByLastName:
            result = (struct Employee*)bsearch (target, employees, count, sizeof(struct Employee) , bsCompareLastName);
            if (result == NULL){
                printf("Employee with last name %s not found in DB\n", target->last_name);
                printf("\n");
            }
            else{
                printEmployee(result);
            }
            break;
        case FindAll:
            for (int i=0;i<count;i++){
                if (strcasecmp(employees[i].last_name, target->last_name) == 0){
                    found = 1;
                    target->six_digit_ID = employees[i].six_digit_ID;
                    strcpy(target->first_name, employees[i].first_name);
                    target->salary = employees[i].salary;
                    printEmployee(target);
                }
            }
            if (!found){
                 printf("Employee with last name %s not found in DB\n", target->last_name);
            }
            break;
        }
        break;
    }
}



