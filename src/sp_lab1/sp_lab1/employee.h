#ifndef EMPLOYEE_H_INCLUDED
#define EMPLOYEE_H_INCLUDED
#define MAXNAME 64
#define MAXARR 1024

struct Employee
{
    int     six_digit_ID;
    char    first_name[MAXNAME];
    char    last_name[MAXNAME];
    int     salary;

};

enum EmployeeField{
    six_digit_ID = 1,
    first_name = 2,
    last_name = 3,
    salary = 4
};

enum Selection{
    Print = 1,
    LookupByID = 2,
    LookupByLastName = 3,
    Add = 4,
    Quit = 5,
    Remove = 6,
    Update = 7,
    HighestSalaries = 8,
    FindAll = 9
};

void loadEmployees(struct Employee *employees, FILE* fp, int *ec);
void buildEmployee(struct Employee *employee, char buff[]);
int substr(char dst[],char src[],int start,int len);
void printEmployees(struct Employee *employees, int count);
int compare(struct Employee *employee1, struct Employee *employee2);
int bsCompare(struct Employee *employee1, struct Employee *employee2);
void sortEmployee(struct Employee employees[], int count, enum EmployeeField field);
void binarySearchEmployee(struct Employee employees[], int count, struct Employee *target, enum EmployeeField field, enum Selection selection);
void addEmployee(struct Employee employees[], FILE* fp, int six_digit_ID, char first_name[], char last_name[], int salary, int index);
void saveEmployee(struct Employee employees[], FILE* fp, int count);
void updateEmployee(struct Employee employees[], struct Employee *employee, int employeeCount);

#endif // EMPLOYEE_H_INCLUDED
